"""add email column in user table

Revision ID: c8daea6a5fc4
Revises: a4d420d3b1b7
Create Date: 2018-08-21 16:51:50.172288

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c8daea6a5fc4'
down_revision = 'a4d420d3b1b7'
branch_labels = None
depends_on = None


def upgrade():
    """add email column into user table when alembic upgrade """
    op.add_column('user', sa.Column('email', sa.VARCHAR(200)))


def downgrade():
    """drop email column from user table when alembic downgrade """
    op.drop_column('user', 'email')
