"""create phone table

Revision ID: bcab287ae341
Revises: c8daea6a5fc4
Create Date: 2018-08-22 11:07:02.682764

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bcab287ae341'
down_revision = 'c8daea6a5fc4'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'phone',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id', sa.Integer, nullable=False),
        sa.Column('number', sa.VARCHAR(20), nullable=False),
        sa.Column('remark', sa.VARCHAR(200))
    )


def downgrade():
    op.drop_table('phone')
