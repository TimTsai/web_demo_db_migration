"""create user table

Revision ID: a4d420d3b1b7
Revises: 
Create Date: 2018-08-21 12:17:30.182791

"""
from alembic import op
import sqlalchemy as sa

revision = 'a4d420d3b1b7'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    """
    when alembic do upgrade, it will create 'user' table
    """

    op.create_table(
        'user',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.VARCHAR(45), nullable=False),
        sa.Column('gender', sa.Boolean, nullable=False)
    )


def downgrade():
    """
    when alembic do downgrade, it will drop 'user' table
    """
    op.drop_table('user')
