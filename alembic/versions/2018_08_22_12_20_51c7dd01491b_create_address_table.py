"""create address table

Revision ID: 51c7dd01491b
Revises: bcab287ae341
Create Date: 2018-08-22 12:20:23.946399

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '51c7dd01491b'
down_revision = 'bcab287ae341'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'address',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id', sa.Integer, nullable=False),
        sa.Column('address', sa.VARCHAR(200), nullable=False),
        sa.Column('remark', sa.VARCHAR(200))
    )


def downgrade():
    op.drop_table('address')
